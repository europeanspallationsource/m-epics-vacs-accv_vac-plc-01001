############################## Vacuum Pumping Group ##############################
##############################      Warm LINAC      ##############################
############################## Version: 4.0.1       ##############################


#-############################
#- COMMAND BLOCK
#-############################

define_command_block()

add_digital("AutoCmd",                                    PV_NAME="AutoModeCmd",                   PV_DESC="Set control mode to AUTOMATIC")
add_digital("ManuCmd",                                    PV_NAME="ManualModeCmd",                 PV_DESC="Set control mode to MANUAL")
add_digital("StopManuCmd",                                PV_NAME="ManualStopCmd",                 PV_DESC="Manual STOP command")
add_digital("StartManuCmd",                               PV_NAME="ManualStartCmd",                PV_DESC="Manual START command")
add_digital() #ForceOnCmd
add_digital("BypassItlCmd",                               PV_NAME="OverrideITLckCmd",              PV_DESC="Override tripped interlock")
add_digital("BlockOffCmd",                                PV_NAME="AutoStartDisCmd",               PV_DESC="Disable automatic START")
add_digital("ResetCmd",                                   PV_NAME="RstErrorsCmd",                  PV_DESC="Reset errors and warnings")
#-----------------------------
add_digital("EnableHighVacuumStart",                      PV_NAME="VacStartEnaCmd",                PV_DESC="Pressure_Sector <= Pressure_Low-Vacuum")
add_digital("EnableLowVacuumStart",                       PV_NAME="ATMStartEnaCmd",                PV_DESC="Pressure_Sector > Pressure_Low-Vacuum")
add_digital("ClearAutoRestartCmd",                        PV_NAME="ClrAutoRestartCmd",             PV_DESC="Disable auto RESTART after some errors")
add_digital("AutoRestartCmd",                             PV_NAME="AutoRestartCmd",                PV_DESC="Enable auto RESTART after some errors")
add_digital() #ClrForceOnCmd
add_digital("ClearBypassCmd",                             PV_NAME="ClrOverrideITLckCmd",           PV_DESC="Clear interlock override")
add_digital("UnlockCmd",                                  PV_NAME="AutoStartEnaCmd",               PV_DESC="Enable automatic START")
add_digital("ClearCountersCmd",                           PV_NAME="ClrCountersCmd",                PV_DESC="Clear counters")

add_analog("ModeCmd",              "BYTE",                PV_NAME="ModeCmd",                       PV_DESC="Automatic mode selection")


#-############################
#- STATUS BLOCK
#-############################

define_status_block()

add_digital("ManuSt",                                     PV_NAME="ManualModeR",                   PV_DESC="Control mode status",                     PV_ONAM="Manual",               PV_ZNAM="Auto")
add_digital("OffSt",                                      PV_NAME="StoppedR",                      PV_DESC="The pumping group is STOPPED",            PV_ONAM="Stopped")
add_digital("AcceleratingSt",                             PV_NAME="AcceleratingR",                 PV_DESC="The pumping group is ACCELERATING",       PV_ONAM="Accelerating")
add_digital("OnSt",                                       PV_NAME="AtNominalSpdR",                 PV_DESC="The pumping group is at NOMINAL SPEED",   PV_ONAM="At Nominal Speed", ARCHIVE=True)
add_digital("ErrorSt",                                    PV_NAME="ErrorR",                        PV_DESC="Error detected by the control function",  PV_ONAM="Error", ARCHIVE=True)
add_digital("EnableLowVacuumStartSt",                     PV_NAME="ATMStartEnaStR",                PV_DESC="ATM start enabled status",                PV_ONAM="Enabled",              PV_ZNAM="Disabled")
add_digital("AutoRestartSt",                              PV_NAME="AutoRestartR",                  PV_DESC="Automatic RESTART toggle status",         PV_ONAM="Auto Restart Enabled", PV_ZNAM="Auto Restart Disabled")
add_digital("InterlockTriggerSt",                         PV_NAME="ITLckTrigR",                    PV_DESC="Interlock triggering status",             PV_ONAM="NominalState",         PV_ZNAM="Tripped", PV_ZSV="MAJOR")
#-----------------------------
add_digital("NominalDQCmd",                               PV_NAME="StartDQ-RB",                    PV_DESC="Status of the START digital output",      PV_ONAM="True",                 PV_ZNAM="False")
add_digital("StartManuSt",                                PV_NAME="OnManualStartR",                PV_DESC="Current START command was MANUAL",        PV_ONAM="Manual Start")
add_digital("StartAutoSt",                                PV_NAME="OnAutoStartR",                  PV_DESC="Current START command was AUTOMATIC",     PV_ONAM="Automatic Start")
add_digital("LockedSt",                                   PV_NAME="AutoStartDisStR",               PV_DESC="Automatic START toggle status",           PV_ONAM="Auto Start Disabled",  PV_ZNAM="Auto Start Enabled")
add_digital("WarningSt",                                  PV_NAME="WarningR",                      PV_DESC="A warning is active",                     PV_ONAM="Warning")
add_digital("InvalidCommandSt",                           PV_NAME="InvalidCommandR",               PV_DESC="Last command sent cannot be processed",   PV_ONAM="Invalid command")
add_digital("Low Vacuum Pumping System Configuration",    PV_NAME="LowVacuumConfigR",              PV_DESC="No turbo pumps configured",               PV_ONAM="No VPT configured")
add_digital("ValidSt",                                    PV_NAME="ValidR",                        PV_DESC="Communication is valid",                  PV_ONAM="Valid",                PV_ZNAM="Invalid")

add_digital("HardWItlHealthySt",                          PV_NAME="ITLck:HW:HltyR",                PV_DESC="Hardware interlock is HEALTHY",           PV_ONAM="HEALTHY")
add_digital("HardwareInterlockSt",                        PV_NAME="ITLck:HW:TrpR",                 PV_DESC="Hardware interlock is TRIPPED",           PV_ONAM="TRIPPED", PV_OSV="MAJOR")
add_digital("HardWItlBypassSt",                           PV_NAME="ITLck:HW:OvRidnR",              PV_DESC="Hardware interlock is OVERRIDEN",         PV_ONAM="OVERRIDEN")
add_digital("Disable Hardware Interlock",                 PV_NAME="ITLck:HW:DisR",                 PV_DESC="Hardware interlock is NOT CONFIGURED",    PV_ONAM="NOT CONFIGURED",       PV_ZNAM="CONFIGURED")
add_digital("SoftWItlHealthySt",                          PV_NAME="ITLck:SW:HltyR",                PV_DESC="Software interlock is HEALTHY",           PV_ONAM="HEALTHY")
add_digital("SoftwareInterlockSt",                        PV_NAME="ITLck:SW:TrpR",                 PV_DESC="Software interlock is TRIPPED",           PV_ONAM="TRIPPED", PV_OSV="MAJOR")
add_digital("SoftWItlBypassSt",                           PV_NAME="ITLck:SW:OvRidnR",              PV_DESC="Software interlock is OVERRIDEN",         PV_ONAM="OVERRIDEN")
add_digital("Disable Software Interlock",                 PV_NAME="ITLck:SW:DisR",                 PV_DESC="Software interlock is NOT CONFIGURED",    PV_ONAM="NOT CONFIGURED",       PV_ZNAM="CONFIGURED")
#-----------------------------
add_digital("2nd Step Active",                            PV_NAME="PumpSys:2:StepValidR",          PV_DESC="High vacuum pumping system 2 controlled", PV_ONAM="Step Active")
add_digital("3rd Step Active",                            PV_NAME="PumpSys:3:StepValidR",          PV_DESC="High vacuum pumping system 3 controlled", PV_ONAM="Step Active")
add_digital("4th Step Active",                            PV_NAME="PumpSys:4:StepValidR",          PV_DESC="High vacuum pumping system 4 controlled", PV_ONAM="Step Active")
add_digital("5th Step Active",                            PV_NAME="PumpSys:5:StepValidR",          PV_DESC="High vacuum pumping system 5 controlled", PV_ONAM="Step Active")
add_digital("High Vacuum Pumping System no2 Configured",  PV_NAME="PumpSys:2:ConfigrdR",           PV_DESC="High vacuum pumping system 2 configured", PV_ONAM="Configured")
add_digital("High Vacuum Pumping System no3 Configured",  PV_NAME="PumpSys:3:ConfigrdR",           PV_DESC="High vacuum pumping system 3 configured", PV_ONAM="Configured")
add_digital("High Vacuum Pumping System no4 Configured",  PV_NAME="PumpSys:4:ConfigrdR",           PV_DESC="High vacuum pumping system 4 configured", PV_ONAM="Configured")
add_digital("High Vacuum Pumping System no5 Configured",  PV_NAME="PumpSys:5:ConfigrdR",           PV_DESC="High vacuum pumping system 5 configured", PV_ONAM="Configured")

add_analog("WarningCode",             "INT",              PV_NAME="WarningCodeR",                  PV_DESC="Active warning code")

add_analog("ErrorCode",               "INT",              PV_NAME="ErrorCodeR",                    PV_DESC="Active error code", ARCHIVE=True)

add_analog("ModeSt",                  "BYTE",             PV_NAME="ModeR",                         PV_DESC="Automatic mode status")

add_analog("ObjectSt",                "BYTE",             PV_NAME="ObjStatCodeR",                  PV_DESC="")

add_analog("1st Active Step Number",  "INT",              PV_NAME="PumpSys:1:StepR",               PV_DESC="Step number of high VAC pumping system 1")

add_analog("2nd Active Step Number",  "INT",              PV_NAME="PumpSys:2:StepR",               PV_DESC="Step number of high VAC pumping system 2")

add_analog("3rd Active Step Number",  "INT",              PV_NAME="PumpSys:3:StepR",               PV_DESC="Step number of high VAC pumping system 3")

add_analog("4th Active Step Number",  "INT",              PV_NAME="PumpSys:4:StepR",               PV_DESC="Step number of high VAC pumping system 4")

add_analog("5th Active Step Number",  "INT",              PV_NAME="PumpSys:5:StepR",               PV_DESC="Step number of high VAC pumping system 5")

add_analog("RunTime",                 "REAL",             PV_NAME="RunTimeR",                      PV_DESC="Runtime in hours", PV_EGU="h", ARCHIVE=True)

add_analog("StartCounter",            "UDINT",            PV_NAME="StartCounterR",                 PV_DESC="Number of starts", ARCHIVE=True)

add_verbatim("""
record(calc, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:iTrpR")
{
	field(DESC, "True if TRIPPED and not OVERRIDEN")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:OvRidnR CP")
	field(CALC, "A && !B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:iTrpR")
{
	field(DESC, "True if TRIPPED and not OVERRIDEN")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:OvRidnR CP")
	field(CALC, "A && !B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:iHltyR")
{
	field(DESC, "True if HEALTHY or DISABLED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:HltyR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:DisR CP")
	field(CALC, "A || B")
}
record(calc, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:iHltyR")
{
	field(DESC, "True if HEALTHY or DISABLED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:HltyR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:DisR CP")
	field(CALC, "A || B")
}

record(calcout, "[PLCF#INSTALLATION_SLOT]:ITLck:iHltyR")
{
	field(DESC, "Interlocks are HEALTHY")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:DisR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:iHltyR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:iHltyR CP")

	field(CALC, "!A && B && C")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLck:HltyR PP")
}
record(bi, "[PLCF#INSTALLATION_SLOT]:ITLck:HltyR")
{
	field(DESC, "Interlocks are HEALTHY")
	field(ONAM, "HEALTHY")
	field(DISP, "1")
}

record(calcout, "[PLCF#INSTALLATION_SLOT]:ITLck:iTrpR")
{
	field(DESC, "At least one interlock is TRIPPED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:iTrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:iTrpR CP")

	field(CALC, "A || B")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLck:TrpR PP")
}
record(bi, "[PLCF#INSTALLATION_SLOT]:ITLck:TrpR")
{
	field(DESC, "At least one interlock is TRIPPED")
	field(OSV,  "MAJOR")
	field(ONAM, "TRIPPED")
	field(DISP, "1")
}

record(calcout, "[PLCF#INSTALLATION_SLOT]:ITLck:iOvRidnR")
{
	field(DESC, "At least one interlock is OVERRIDEN")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:OvRidnR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:OvRidnR CP")

	field(CALC, "!A && (B || C)")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLck:OvRidnR PP")
}
record(bi, "[PLCF#INSTALLATION_SLOT]:ITLck:OvRidnR")
{
	field(DESC, "At least one interlock is OVERRIDEN")
	field(ONAM, "OVERRIDEN")
	field(DISP, "1")
}

record(calcout, "[PLCF#INSTALLATION_SLOT]:ITLck:iDisR")
{
	field(DESC, "Interlocks are NOT CONFIGURED")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:HW:DisR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:SW:DisR CP")

	field(CALC, "A && B")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLck:DisR PP")
}
record(bi, "[PLCF#INSTALLATION_SLOT]:ITLck:DisR")
{
	field(DESC, "Interlocks are NOT CONFIGURED")
	field(ONAM, "NOT CONFIGURED")
	field(ZNAM, "CONFIGURED")
	field(DISP, "1")
}


record(calcout, "[PLCF#INSTALLATION_SLOT]:iITLckStatR")
{
	field(DESC, "Calculate combined interlock status")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:ITLck:TrpR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:ITLck:OvRidnR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:ITLck:HltyR CP")
	field(INPD, "[PLCF#INSTALLATION_SLOT]:ITLck:DisR CP")

# Check that only one is true
	field(CALC, "E:=(A | B*2 | C*4 | D*8);F:=!(E&(E-1));F")
	field(OCAL, "F?(C + A*2 + B*3 + D*4):0")
	field(DOPT, "Use OCAL")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:ITLckStatR PP")
}
record(mbbi, "[PLCF#INSTALLATION_SLOT]:ITLckStatR")
{
	field(DESC, "Combined Interlock status")
	field(ZRST, "INVALID")
	field(ONST, "HEALTHY")
	field(TWST, "TRIPPED")
	field(TWSV, "MAJOR")
	field(THST, "OVERRIDEN")
	field(FRST, "NOT CONFIGURED")
}
""")

add_verbatim("""
record(calcout, "[PLCF#INSTALLATION_SLOT]:iStatR")
{
	field(DESC, "Calculate pumping group status")
	field(INPA, "[PLCF#INSTALLATION_SLOT]:AcceleratingR CP")
	field(INPB, "[PLCF#INSTALLATION_SLOT]:AtNominalSpdR CP")
	field(INPC, "[PLCF#INSTALLATION_SLOT]:StoppedR CP")
	field(INPD, "[PLCF#INSTALLATION_SLOT]:ErrorR CP")
	field(INPE, "[PLCF#INSTALLATION_SLOT]:ValidR CP MSS")

# Check that only one is true. Error is a bit special though
	field(CALC, "F:=(A | B*2 | C*4 | D*8);G:=!(F&(F-1));G")
	field(OCAL, "E?(G?(A + B*2 + C*3 + D*4):D*4):0")
	field(DOPT, "Use OCAL")
	field(OUT,  "[PLCF#INSTALLATION_SLOT]:StatR PP MSS")
}
record(mbbi, "[PLCF#INSTALLATION_SLOT]:StatR")
{
	field(DESC, "Pumping group status")
	field(ZRST, "INVALID")
	field(ONST, "ACCELERATING")
	field(TWST, "AT NOMINAL SPEED")
	field(THST, "STOPPED")
	field(FRST, "ERROR")
	field(FRSV, "MAJOR")
}
""")


#-############################
#- METADATA
#-############################
#- ERRORS
define_metadata("error", MD_SCRIPT='css_code2msg.py', CODE_TYPE='error')
#-
add_metadata(99, 'Hardware Interlock')
add_metadata(98, 'Software Interlock')
add_metadata(97, 'Turbo-Pumps Controller Error')
add_metadata(96, 'Power Supply Error')
add_metadata(95, 'High Vacuum Manifold Vented - Pressure Interlock')
add_metadata(94, 'Low Vacuum Manifold Vented - Pressure Interlock')
add_metadata(92, 'Primary Pump Error')
add_metadata(91, 'Primary Pump / Valve Error')
add_metadata(90, 'Turbo-Pumps Not Available')
add_metadata(89, 'Max Auto-Restart')
add_metadata(88, 'Back-Up Primary Pumping System Error')
add_metadata(15, 'Mode Selection Error - Vacuum Sector Vented')
add_metadata(14, 'Mode Selection Error - Vacuum Sector Under Vacuum')
add_metadata(13, 'Primary Pumping System: Rescue Primary System is Off')
add_metadata(12, 'Primary Pumping System: Rescue Primary Not Available')
add_metadata(11, 'Primary Pumping System: Primary Pump / Valve Error')
add_metadata(10, 'Primary Pumping System: Start Disable')
add_metadata( 9, 'VPSU-0020 400VAC 3-Phases Power Supply Tripped')
#-
#- WARNINGS
define_metadata("warning", MD_SCRIPT='css_code2msg.py', CODE_TYPE='warning')
#-
add_metadata(99, 'Service Mode (Leak Detection) Over Time')
add_metadata(19, 'High Vacuum Pumping Starting Not Possible - All Systems are Locked')
add_metadata(18, '1st High Vacuum Pumping System Error - Angle Valve Position Error')
add_metadata(17, '2nd High Vacuum Pumping System Error - Angle Valve Position Error')
add_metadata(16, '3rd High Vacuum Pumping System Error - Angle Valve Position Error')
add_metadata(15, '4th High Vacuum Pumping System Error - Angle Valve Position Error')
add_metadata(14, '5th High Vacuum Pumping System Error - Angle Valve Position Error')
add_metadata(11, 'Atmospheric & Vacuum Starting are Enabled')
add_metadata(10, 'No Starting Enabled')
add_metadata( 9, 'Atmospheric Starting is Enabled')
add_metadata( 8, 'VPSU-0020 24VDC Power Supply Overload')
add_metadata( 7, 'VPSU-0020 24VDC Power Supply Tripped')
add_metadata( 6, 'VPSU-0010 24VDC Power Supply Overload')
add_metadata( 5, 'VPSU-0010 24VDC Power Supply Tripped')
add_metadata( 4, 'VPSU-0010 400VAC 3-Phases Power Supply Tripped')
add_metadata( 3, 'Automatic Restart On-going')
add_metadata( 2, 'All Pumping Systems Locked - Starting Not Possible')
add_metadata( 1, 'No Mode Selected')
#-
#- MODES
define_metadata("mode")
#-
add_metadata(10, "Manual Mode")
add_metadata( 8, "Venting")
add_metadata( 7, "Leak Detection - Manual Mode")
add_metadata( 6, "Leak Detection - Partial Flow")
add_metadata( 5, "Leak Detection")
add_metadata( 3, "Pump Down - Atmospheric Pressure")
add_metadata( 1, "Normal Pumping")
add_metadata( 0, "Not Used")
#-
#- STEPS
define_metadata("step")
#- Step Number  "Alias To Be Dispaled - Step Name"                      "Step Name"
#- -----------  ----------------------------------                      -----------
add_metadata(  1, "VPG OFF (Init Step)",                                "INITIAL_STEP")
add_metadata(  2, "Starting Low Vacuum",                                "Primary_Pumping_Transition@START")
add_metadata(  3, "Low Vacuum Devices Locked",                          "Check_VPP1@START")
add_metadata(  4, "Primary Pump Starting",                              "Start_VPP1@START")
add_metadata(  5, "Primary Pump Accelerating",                          "VPP1_Acceleration@START")
add_metadata(  6, "Low Vacuum Valve Opening",                           "Open_VVA_VPP1@START")
add_metadata(  7, "Low Vacuum Pumping On-Going",                        "VPP1_Stabilisation@START")
add_metadata(  8, "Not Used",                                           "Not Used")
add_metadata(  9, "Not Used",                                           "Not Used")
add_metadata( 10, "Starting High Vacuum",                               "Turbo_Pumping_Transition@START")
add_metadata( 11, "High Vacuum Devices Locked",                         "Check_VPT1@START")
add_metadata( 12, "Starting Delay",                                     "Starting_Delay_VPT1@START")
add_metadata( 13, "High Vacuum Pump Starting",                          "Start_VPT1@START")
add_metadata( 14, "High Vacuum Pump Accelerating",                      "VPT1_Acceleration@START")
add_metadata( 15, "High Vacuum Valve Opening",                          "Open-VV_VPT1@START")
add_metadata( 16, "High Vacuum Pumping On-Going",                       "VPT1_Stabilisation@START")
add_metadata( 17, "High Vacuum Pumping Error",                          "VPT1_Error@START")
add_metadata( 18, "Not Used",                                           "Not Used")
add_metadata( 19, "Not Used",                                           "Not Used")
add_metadata( 20, "Not Used",                                           "Not Used")
add_metadata( 21, "High Vacuum Devices Locked",                         "Check_VPT2@START")
add_metadata( 22, "Starting Delay",                                     "Starting_Delay_VPT2@START")
add_metadata( 23, "High Vacuum Pump Starting",                          "Start_VPT2@START")
add_metadata( 24, "High Vacuum Pump Accelerating",                      "VPT2_Acceleration@START")
add_metadata( 25, "High Vacuum Valve Opening",                          "Open-VV_VPT2@START")
add_metadata( 26, "High Vacuum Pumping On-Going",                       "VPT2_Stabilisation@START")
add_metadata( 27, "High Vacuum Pumping Error",                          "VPT2_Error@START")
add_metadata( 28, "Not Used",                                           "Not Used")
add_metadata( 29, "Not Used",                                           "Not Used")
add_metadata( 30, "Not Used",                                           "Not Used")
add_metadata( 31, "High Vacuum Devices Locked",                         "Check_VPT3@START")
add_metadata( 32, "Starting Delay",                                     "Starting_Delay_VPT3@START")
add_metadata( 33, "High Vacuum Pump Starting",                          "Start_VPT3@START")
add_metadata( 34, "High Vacuum Pump Accelerating",                      "VPT3_Acceleration@START")
add_metadata( 35, "High Vacuum Valve Opening",                          "Open-VV_VPT3@START")
add_metadata( 36, "High Vacuum Pumping On-Going",                       "VPT3_Stabilisation@START")
add_metadata( 37, "High Vacuum Pumping Error",                          "VPT3_Error@START")
add_metadata( 38, "Not Used",                                           "Not Used")
add_metadata( 39, "Not Used",                                           "Not Used")
add_metadata( 40, "Not Used",                                           "Not Used")
add_metadata( 41, "High Vacuum Devices Locked",                         "Check_VPT4@START")
add_metadata( 42, "Starting Delay",                                     "Starting_Delay_VPT4@START")
add_metadata( 43, "High Vacuum Pump Starting",                          "Start_VPT4@START")
add_metadata( 44, "High Vacuum Pump Accelerating",                      "VPT4_Acceleration@START")
add_metadata( 45, "High Vacuum Valve Opening",                          "Open-VV_VPT4@START")
add_metadata( 46, "High Vacuum Pumping On-Going",                       "VPT4_Stabilisation@START")
add_metadata( 47, "High Vacuum Pumping Error",                          "VPT4_Error@START")
add_metadata( 48, "Not Used",                                           "Not Used")
add_metadata( 49, "Not Used",                                           "Not Used")
add_metadata( 50, "Not Used",                                           "Not Used")
add_metadata( 51, "High Vacuum Devices Locked",                         "Check_VPT5@START")
add_metadata( 52, "Starting Delay",                                     "Starting_Delay_VPT5@START")
add_metadata( 53, "Starting High Vacuum Pump",                          "Start_VPT5@START")
add_metadata( 54, "High Vacuum Pump Accelerating",                      "VPT5_Acceleration@START")
add_metadata( 55, "High Vacuum Valve Opening",                          "Open-VV_VPT5@START")
add_metadata( 56, "High Vacuum Pumping On-Going",                       "VPT5_Stabilisation@START")
add_metadata( 57, "High Vacuum Pumping Error",                          "VPT5_Error@START")
add_metadata( 58, "Not Used",                                           "Not Used")
add_metadata( 59, "Not Used",                                           "Not Used")
add_metadata( 60, "Not Used",                                           "Not Used")
add_metadata( 61, "High Vacuum Pumping Transition",                     "Turbo-Pumping-Transition-To-NOMINAL@START")
add_metadata( 62, "Nominal",                                            "NOMINAL")
add_metadata( 63, "Not Used",                                           "Not Used")
add_metadata( 64, "Not Used",                                           "Not Used")
add_metadata( 65, "Stopping High Vacuum Pumping",                       "Stop-VPTs-Transition@STOP")
add_metadata( 66, "High Vacuum Devices Locked",                         "Check_VPT1@STOP")
add_metadata( 67, "Valve Closing",                                      "Close_VV_VPT1@STOP")
add_metadata( 68, "High Vacuum Pump Stopping",                          "Stop_VPT1@STOP")
add_metadata( 69, "Valve Closing",                                      "SPARE_Step2_VPT1@STOP")
add_metadata( 70, "High Vacuum Devices Locked",                         "Check_VPT2@STOP")
add_metadata( 71, "Valve Closing",                                      "Close_VV_VPT2@STOP")
add_metadata( 72, "High Vacuum Pump Stopping",                          "Stop_VPT2@STOP")
add_metadata( 73, "Valve Closing",                                      "SPARE_Step2_VPT2@STOP")
add_metadata( 74, "High Vacuum Devices Locked",                         "Check_VPT3@STOP")
add_metadata( 75, "Valve Closing",                                      "Close_VV_VPT3@STOP")
add_metadata( 76, "High Vacuum Pump Stopping",                          "Stop_VPT3@STOP")
add_metadata( 77, "Valve Closing",                                      "SPARE_Step2_VPT3@STOP")
add_metadata( 78, "High Vacuum Devices Locked",                         "Check_VPT4@STOP")
add_metadata( 79, "Valve Closing",                                      "Close_VV_VPT4@STOP")
add_metadata( 80, "High Vacuum Pump Stopping",                          "Stop_VPT4@STOP")
add_metadata( 81, "Valve Closing",                                      "SPARE_Step2_VPT4@STOP")
add_metadata( 82, "High Vacuum Devices Locked",                         "Check_VPT5@STOP")
add_metadata( 83, "Valve Closing",                                      "Close_VV_VPT5@STOP")
add_metadata( 84, "High Vacuum Pump Stopping",                          "Stop_VPT5@STOP")
add_metadata( 85, "Valve Closing",                                      "SPARE_Step2_VPT5@STOP")
add_metadata( 86, "Stopping Low Vacuum Pumping",                        "STOP_VPP_Transition@STOP")
add_metadata( 87, "Valve Closing",                                      "Close_VVA-VPP1@STOP")
add_metadata( 88, "Low Vacuum Pump Stopping",                           "Stop_VPP1@STOP")
add_metadata( 89, "Not Used",                                           "Not Used")
add_metadata( 90, "Not Used",                                           "Not Used")
add_metadata( 91, "Not Used",                                           "Not Used")
add_metadata( 92, "Not Used",                                           "Not Used")
add_metadata( 93, "Not Used",                                           "Not Used")
add_metadata( 94, "Turbo-Pumps Venting - Delay",                        "Delay-To-Vent@STOP")
add_metadata( 95, "Turbo Pumps Venting",                                "Venting-Ongoing@STOP")
add_metadata( 96, "Turbo Pumps Venting - Valves Closing",               "End-Of-Venting@STOP")
add_metadata( 97, "Not Used",                                           "Not Used")
add_metadata( 98, "Not Used",                                           "Not Used")
add_metadata( 99, "Not Used",                                           "Not Used")
add_metadata(100, "Not Used",                                           "Not Used")
add_metadata(101, "Not Used",                                           "Not Used")
add_metadata(102, "Valve's Interlocks Overriding",                      "Override_Valves_Interlocks@PUMP-DOWN")
add_metadata(103, "Valves Opening",                                     "Open_Valves@PUMP-DOWN")
add_metadata(104, "Starting Low Vacuum",                                "Primary_Pumping_Transition@PUMP-DOWN")
add_metadata(105, "Low Vacuum Devices Locked",                          "Check_VPP1@PUMP-DOWN")
add_metadata(106, "Primary Pump Starting",                              "Start_VPP1@PUMP-DOWN")
add_metadata(107, "Primary Pump Accelerating",                          "VPP1_Acceleration@PUMP-DOWN")
add_metadata(108, "Low Vacuum Valve Opening",                           "Open_VVA_VPP1@PUMP-DOWN")
add_metadata(109, "Low Vacuum Pumping On-Going",                        "VPP1_Stabilisation@PUMP-DOWN")
add_metadata(110, "Starting High Vacuum",                               "Turbo_Pumping_Transition@PUMP-DOWN")
add_metadata(111, "High Vacuum Devices Locked",                         "Check_VPT1@PUMP-DOWN")
add_metadata(112, "High Vacuum Pump Starting",                          "Start_VPT1@PUMP-DOWN")
add_metadata(113, "High Vacuum Pump Accelerating",                      "VPT1_Acceleration@PUMP-DOWN")
add_metadata(114, "High Vacuum Pumping Delay",                          "VPT1_NominalSpeed@PUMP-DOWN")
add_metadata(115, "High Vacuum Pump Error",                             "VPT1_Error@PUMP-DOWN")
add_metadata(116, "Not Used",                                           "Not Used")
add_metadata(117, "Not Used",                                           "Not Used")
add_metadata(118, "Not Used",                                           "Not Used")
add_metadata(119, "Not Used",                                           "Not Used")
add_metadata(120, "Not Used",                                           "Not Used")
add_metadata(121, "High Vacuum Devices Locked",                         "Check_VPT2@PUMP-DOWN")
add_metadata(122, "High Vacuum Pump Starting",                          "Start_VPT2@PUMP-DOWN")
add_metadata(123, "High Vacuum Pump Accelerating",                      "VPT2_Acceleration@PUMP-DOWN")
add_metadata(124, "High Vacuum Pumping Delay",                          "VPT2_NominalSpeed@PUMP-DOWN")
add_metadata(125, "High Vacuum Pump Error",                             "VPT2_Error@PUMP-DOWN")
add_metadata(126, "Not Used",                                           "Not Used")
add_metadata(127, "Not Used",                                           "Not Used")
add_metadata(128, "Not Used",                                           "Not Used")
add_metadata(129, "Not Used",                                           "Not Used")
add_metadata(130, "Not Used",                                           "Not Used")
add_metadata(131, "High Vacuum Devices Locked",                         "Check_VPT3@PUMP-DOWN")
add_metadata(132, "High Vacuum Pump Starting",                          "Start_VPT3@PUMP-DOWN")
add_metadata(133, "High Vacuum Pump Accelerating",                      "VPT3_Acceleration@PUMP-DOWN")
add_metadata(134, "High Vacuum Pumping Delay",                          "VPT3_NominalSpeed@PUMP-DOWN")
add_metadata(135, "High Vacuum Pump Error",                             "VPT3_Error@PUMP-DOWN")
add_metadata(136, "Not Used",                                           "Not Used")
add_metadata(137, "Not Used",                                           "Not Used")
add_metadata(138, "Not Used",                                           "Not Used")
add_metadata(139, "Not Used",                                           "Not Used")
add_metadata(140, "Not Used",                                           "Not Used")
add_metadata(141, "High Vacuum Devices Locked",                         "Check_VPT4@PUMP-DOWN")
add_metadata(142, "High Vacuum Pump Starting",                          "Start_VPT4@PUMP-DOWN")
add_metadata(143, "High Vacuum Pump Accelerating",                      "VPT4_Acceleration@PUMP-DOWN")
add_metadata(144, "High Vacuum Pumping Delay",                          "VPT4_NominalSpeed@PUMP-DOWN")
add_metadata(145, "High Vacuum Pump Error",                             "VPT4_Error@PUMP-DOWN")
add_metadata(146, "Not Used",                                           "Not Used")
add_metadata(147, "Not Used",                                           "Not Used")
add_metadata(148, "Not Used",                                           "Not Used")
add_metadata(149, "Not Used",                                           "Not Used")
add_metadata(150, "Not Used",                                           "Not Used")
add_metadata(151, "High Vacuum Devices Locked",                         "Check_VPT5@PUMP-DOWN")
add_metadata(152, "High Vacuum Pump Starting",                          "Start_VPT5@PUMP-DOWN")
add_metadata(153, "High Vacuum Pump Accelerating",                      "VPT5_Acceleration@PUMP-DOWN")
add_metadata(154, "High Vacuum Pumping Delay",                          "VPT5_NominalSpeed@PUMP-DOWN")
add_metadata(155, "High Vacuum Pump Error",                             "VPT5_Error@PUMP-DOWN")
add_metadata(156, "Not Used",                                           "Not Used")
add_metadata(157, "Not Used",                                           "Not Used")
add_metadata(158, "Not Used",                                           "Not Used")
add_metadata(159, "Not Used",                                           "Not Used")
add_metadata(160, "Not Used",                                           "Not Used")
add_metadata(161, "High Vacuum Pumping Transition",                     "Turbo-Pumping-Transition@PUMP-DOWN")
add_metadata(162, "High Vacuum Pumping Transition (Nominal)",           "Turbo-Pumping-Transition-To-NOMINAL@PUMP-DOWN")
add_metadata(163, "Not Used",                                           "Not Used")
add_metadata(164, "Not Used",                                           "Not Used")
add_metadata(165, "Not Used",                                           "Not Used")
add_metadata(166, "Not Used",                                           "Not Used")
add_metadata(167, "Not Used",                                           "Not Used")
add_metadata(168, "Not Used",                                           "Not Used")
add_metadata(169, "Not Used Yet - You should not be there...",          "Not Used")
add_metadata(170, "Not Used Yet - You should not be there...",          "Manual-LD@LEAK-DETECTION")
add_metadata(171, "Not Used Yet - You should not be there...",          "Set-Up_Delay@LEAK-DETECTION")
add_metadata(172, "Not Used Yet - You should not be there...",          "Close_VVA_VPP1@LEAK-DETECTION")
add_metadata(173, "Not Used Yet - You should not be there...",          "Leak_Detection_Ongoing@LEAK-DETECTION")
add_metadata(174, "Not Used Yet - You should not be there...",          "Leak_Detection_End@LEAK-DETECTION")
add_metadata(175, "Not Used Yet - You should not be there...",          "Set-Up_Delay@LARGE-LEAK-DETECTION")
add_metadata(176, "Not Used Yet - You should not be there...",          "Large-Leak_Detection_Ongoing@LEAK-DETECTION")
add_metadata(400, "Valve's Interlocks Overriding",                      "Override_Valves_Interlocks@START")
add_metadata(401, "Pumping Low Vacuum",                                 "Pumping_Low_Vacuum@START")
